#!/usr/bin/env python
from html.parser import HTMLParser
from urllib.request import Request, urlopen
import re, html, os.path

scrap_dir = 'scrapped-data/'
url="http://bulbapedia.bulbagarden.net/wiki/List_of_Pok%C3%A9mon_by_National_Pok%C3%A9dex_number"

class BulbapediaParser(HTMLParser):
  def __init__(self):
    super().__init__()
    self.imagedata = {}
  def handle_starttag(self, tag, attrs):
    if tag == 'img':
      try:
        filename = attrs[0][1]
        image_url = attrs[1][1]
        width = attrs[2][1]
        height = attrs[3][1]
        if width == height and width == '40':
          self.imagedata[filename] = image_url
      except Exception as e:
        # The tag is an image tag, but it doesn't match the standard pokemon images, so skip it.
        pass

class Scrapper:
  def __init__(self, parser, source_url):
    self.parser = parser
    self.source_url = source_url
  def execute(self):
    data = self.buildRequest(self.source_url)
    imagedata = self.parseRequest(self.parser, data)
    self.scrap_images(imagedata)
  def buildRequest(self, source_url):
    print("Downloading {}".format(source_url))
    req = Request(source_url, headers={'User-Agent': 'Mozilla/5.0'})
    data = urlopen(req).read()
    return data
  def parseRequest(self, parser, data):
    parser.feed(data.decode())
    parser.close()
    return parser.imagedata
  def scrap_images(self, imagedata):
    for image in imagedata:
      image_url='https:{}'.format(imagedata[image])
      file_path = '{}{}.png'.format(scrap_dir, image)
      if not os.path.isfile(file_path):
        print("Downloading {} from {}".format(image, image_url))
        img = self.buildRequest(image_url)
        with open(file_path, 'wb') as f:
          f.write(img)
      else:
        print("Previously cached {}".format(image))
    print("Scrapped all image sprites from {}".format(self.source_url))

if __name__ == "__main__":
  parser=BulbapediaParser()
  scrapper = Scrapper(parser, url)
  scrapper.execute()
