#!/bin/bash
if [[ "$(whoami)" != "root" ]]; then
  echo "Root/Sudo is needed to copy/remove lolcow to/from the /usr/local/bin/ directory."
  echo "If you don't like that, I'm sure you're smart enough to figure something out."
  exit 1
fi
if [[ $1 == '-r' ]] || [[ $1 == '--remove' ]]; then
  rm /usr/local/bin/lolcow
  echo -lolcow \"uninstalled\"-
else
  cp lolcow /usr/local/bin/lolcow
  echo -lolcow \"installed\"-
  echo Move the cows by yourself because if you are like me,
  echo you never wanted to mix your cute cows with those ascii ones anyways.
fi
