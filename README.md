LolCow
==========

LolCow is just a simple wrapper for cowsay and lolcat to rainbow-ify the text bubble and keep the cow unaltered. Primarily for cow files that utilize background terminal colors (like when using img2xterm).

![Add kawaii to your terminal](preview.png)

## Installation

```bash
$ git clone http://gitlab.com/jurassicplayer/lolcow
$ cd lolcow
$ sudo ./install.sh
```

Keep in mind that `lolcow` will only work if you have `cowsay` and `lolcat` installed and available in your `$PATH`.

## Usage

You can use lolcow just like cowsay. There is just ONE extra option, which is `-t` in order to use cowthink instead of cowsay.

Usage: lolcow [-t] [cowsay options]

```bash
$ lolcow Hello World
```

To have a random cow saying a random fortune to you, use `fortune`:

```bash
$ fortune | lolcow -f "$(find <path/to/cows> -type f | shuf -n 1)"
```

## Uninstall

You can uninstall lolcow by running:

```bash
$ sudo ./install.sh -r
```

## Extras

You will need [`img2xterm`][img2xterm] to generate ".cow files" from images. ImageMagick is also nice if you want to fix images (flips images and trims transparent areas)

## Mostly taken from [`pokemonsay`][pokemonsay]

If you want to download the images ala pokemonsay or make some change in the process, here is how it's done:

```bash
# Download pokémon images from Bulbapedia... Thanks bulbapedia!
$ ./tools/scrap_data.py

# Manipulate the downloaded images, to make the pokémon look
# to the right, and trim the useless space around them.
$ ./tools/fix_images.sh

# Use 'img2xterm' to generate .cow files (for 'cowsay').
$ ./tools/make_cows.sh
```

## NOTICE

I don't own any artwork...I'll eventually make a list of the spriters when I get some time.

[img2xterm]: https://github.com/rossy/img2xterm
[cowsay]: https://en.wikipedia.org/wiki/Cowsay
[pokemonsay]: https://github.com/possatti/pokemonsay
[lolcat]: https://github.com/busyloop/lolcat
